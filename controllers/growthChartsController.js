const path = require('path');
const  { createGrowthReportXlsx, cleanupFiles } = require('../services/growthChartsService');

exports.getFormData = (req, res) => {
  var renderValues = {
      title: 'Generar reporte',
      idLabel: 'DNI',
      chartTypeLabel: 'Tipo de reporte',
      chartTypes: [
        { id: 1, value: 'Peso' },
        { id: 2, value: 'Longitud/Estatura' },
      ],
      submitButton: 'Enviar'
  }
  return renderValues;
}

exports.generateChartReport = async (req, res) => {
  const { personalId, chartType } = req.body;
  const response = await getGrowthReport(personalId, chartType);
  if (response.status === 200) {
    try {
      setTimeout(() => {
        res.sendFile(
          path.join(__dirname, '../pediatric_charts_who.pdf')
        );
        // cleanupFiles();
      }, 5000);
    } catch (error) {
      console.error(error.message);
      res.status(400).json({ message: error.message }); 
    }
  } else {
    res.status(400).json({ message: response.message })
  }
}

const getGrowthReport = async (id, measureId) => {
  let procedureName;
  if (measureId == '1') {
    procedureName = 'sp_get_weight_who';
  } else {
    procedureName = 'sp_get_length_who';
  }
  const response =  await createGrowthReportXlsx(id, procedureName);
  return response;
}