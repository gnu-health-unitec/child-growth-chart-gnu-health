const exceljs = require('exceljs');
const path = require('path');
const fs = require('fs');
const { spawn } = require('child_process');
const { setTimeout, clearTimeout } = require('timers');
const knex = require('knex')({
  client: "pg",
  connection: {
    connectionString: process.env.PG_CONNECTION_STRING,
    ssl: false
  },
  searchPath: ["knex", "public"],
});

const xlsxDocPath = path.resolve(__dirname, process.env.TMP_XSLS_PATH);
const pythonScriptPath = path.resolve(__dirname, process.env.PLOT_SCRIPT_PATH);

const getSqlCall = async (patientId, procedureName) => {
    const query = `select * from ${procedureName}(?)`;
    const response = await knex.raw(query, [patientId]);
    return response;
}

const getPatientEvaluationData = async (patientId, procedureName) => {
    const response = await getSqlCall(patientId, procedureName);
    if (response.rows.length > 0) {
      return response.rows;
    } else {
      return { status: 400, message: `No evaluation info found for patient ${patientId}` };
    }
}
  
const getPatientData = async (patientId) => {
    const procedureName = `sp_get_data_patient_growth`;
    const response = await getSqlCall(patientId, procedureName);
    if (response.rows.length > 0) {
        return response.rows[0];
    }
    return {};
}
  
const getFilepath = async (procedureName, age_in_days, gender) => {
    let filepath, title;
    let is_monthly = true;
    if (procedureName == 'sp_get_length_who') {
        if (age_in_days <= 91) {
          if (gender == 'm') {
              filepath = '../excel/tab_lhfa_boys_p_0_13.xlsx';
          } else {
              filepath = '../excel/tab_lhfa_girls_p_0_13.xlsx';
          }
          is_monthly = false;
        } else if (age_in_days <= 731) {
          if (gender == 'm') {
              filepath = '../excel/tab_lhfa_boys_p_0_2.xlsx';
          } else {
              filepath = '../excel/tab_lhfa_girls_p_0_2.xlsx';
          }
        } else if (age_in_days <= 1826) {
          if (gender == 'm') {
              filepath = '../excel/tab_lhfa_boys_p_2_5.xlsx';
          } else {
              filepath = '../excel/tab_lhfa_girls_p_2_5.xlsx';
          }
        } else {
          return { status: 400, message: 'Age not in scope' };
        }
        title = 'Gráfica de longitud/estatura';
    } else {
        if (age_in_days <= 91) {
          if (gender == 'm') {
              filepath = '../excel/tab_wfa_boys_p_0_13.xlsx';
          } else {
              filepath = '../excel/tab_wfa_girls_p_0_13.xlsx';
          }
          is_monthly = false;
        } else if (age_in_days <= 731) {
          if (gender == 'm') {
              filepath = '../excel/tab_wfa_boys_p_0_5.xlsx';
          } else {
              filepath = '../excel/tab_wfa_girls_p_0_5.xlsx';
          }
        } else {
          return { status: 400, message: 'Age not in scope' };
        }
        title = 'Gráfica de peso';
    }

    return { filepath, is_monthly, title };
}
  
const writeInWorkbook = async (data, filepath, is_monthly) => {
    try {
        const absolutePath = path.resolve(__dirname, filepath);
        const workbook = new exceljs.Workbook();
        await workbook.xlsx.readFile(absolutePath);
        const worksheet = workbook.getWorksheet(1);

        let yValues = ['Y'];
        worksheet.eachRow((row, rowNumber) => {
            if (rowNumber > 1) {
                const firstColumnValue = row.getCell(1).value;
                const currentDataRow = data.find((element) => {
                    return is_monthly ? (element.month_difference == firstColumnValue) : (element.week_difference == firstColumnValue); 
                });
                if (currentDataRow !== undefined) {
                    console.log(currentDataRow)
                    const currentValue = currentDataRow.hasOwnProperty('height') ? currentDataRow.height : currentDataRow.weight;
                    yValues.push(currentValue);
                } else {
                    yValues.push(null);
                }
            }
        });

        const lastColumnIndex = worksheet.columns.filter(col => !col.hidden).length;
        worksheet.spliceColumns(lastColumnIndex + 1, 1, yValues);

        await fs.promises.copyFile(absolutePath, xlsxDocPath);
        await workbook.xlsx.writeFile(xlsxDocPath);
        return { status: 200 };
    } catch (error) {
        return { status: 400, message: `${error.message} ${error.lineNumber}` }
    }
}
  
const runPlotScript = async (id, fullName, age, title, timeoutMs = 5000) => { // Default timeout of 5 seconds
  const docTitle = `${fullName} (${id} Edad: ${age})\n ${title}` 
  const arguments = ['--path', xlsxDocPath];
  arguments.push('--title', docTitle);
  console.log(arguments);
  const child = spawn('python3', [pythonScriptPath, ...arguments]);

  let timeoutId;

  try {
    timeoutId = setTimeout(() => {
      console.error('Process timed out!');
      child.kill();
    }, timeoutMs);

    clearTimeout(timeoutId);

    console.log('child process exited successfully');

    return { status: 200 };

  } catch (error) {
    console.error(`process error: ${error.message}`);
    return { status: 400, message: error.message };
  } finally {
    clearTimeout(timeoutId);
  }
};
  
exports.cleanupFiles = async () => {
    try {
      fs.unlink(xlsxDocPath, (err) => {
        if (err) throw err;
      });
      // const pdfPath = path.resolve(__dirname, process.env.PDF_FILE_PATH);
      // fs.unlink(pdfPath, (err) => {
      //   if (err) throw err;
      // });
      console.log(`Files deleted successfully`);
    } catch (error) {
      console.error(error.message);
    }
}
  
exports.createGrowthReportXlsx = async (patientId, procedureName) => {
    const evaluationDataResponse = await getPatientEvaluationData(patientId, procedureName);
    const patientData = await getPatientData(patientId);
    const { full_name, age_in_days, age_in_months, age_in_years, gender } = patientData;
    if (evaluationDataResponse.length > 0) {
      const { filepath, is_monthly, title } = await getFilepath(procedureName, age_in_days, gender);
      const writeFile = await writeInWorkbook(evaluationDataResponse, filepath, is_monthly);
      if (writeFile.status === 200) {
        const plotScriptExecution = await runPlotScript(patientId, full_name, age_in_years, title);
        if (plotScriptExecution.status === 200) {
          return plotScriptExecution;
        } else {
          return { status: plotScriptExecution.status, message: plotScriptExecution.message };
        }
      } else {
        return { status: 400, message: writeFile.message };
      }
    } else {
      return { status: 400, message: `No info found for patient ${patientId}`}
    }
}