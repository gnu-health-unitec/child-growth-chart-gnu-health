var express = require('express');
var router = express.Router();
var growthChartsController = require('../controllers/growthChartsController');

/* GET growth charts view. */
router.get('/', function(req, res, next) {
  const renderValues = growthChartsController.getFormData(req, res);
  res.render('growthCharts', renderValues);
});

router.post('/generate_pdf', growthChartsController.generateChartReport);

module.exports = router;
